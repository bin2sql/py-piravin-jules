SET MARKUP HTML ON SPOOL ON PREFORMAT OFF ENTMAP ON -
HEAD "<TITLE>Report</TITLE> -
<STYLE type='text/css'> -
<!-- BODY {background: #000000} --> -
</STYLE>" -
BODY "TEXT='#00FF00'" -
TABLE "WIDTH='90%' BORDER='5'"

SPOOL report_06.htm

SELECT
(SELECT COUNT(employee_id) FROM t_employee) "Employ�s",
(SELECT COUNT(customer_id) FROM t_customer) "Clients",
(SELECT ROUND((COUNT(pass_date)/81)*100) FROM t_customer WHERE MONTHS_BETWEEN(SYSDATE,pass_date) < '12' ) as "% Abonnement",
(SELECT COUNT(train_id) FROM t_train) "Trains",
(SELECT COUNT(station_id) FROM t_station) "Gares",
(SELECT COUNT(reservation_id) FROM t_reservation) "R�servations",
(SELECT COUNT(ticket_id) FROM t_ticket) "Billets"
FROM dual
/

EXIT