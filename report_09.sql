SET MARKUP HTML ON SPOOL ON PREFORMAT OFF ENTMAP ON -
HEAD "<TITLE>Report</TITLE> -
<STYLE type='text/css'> -
<!-- BODY {background: #000000} --> -
</STYLE>" -
BODY "TEXT='#00FF00'" -
TABLE "WIDTH='90%' BORDER='5'"

SPOOL report_09.htm

SELECT train_id , CONCAT(  (ROUND(distance/(TO_CHAR((arrival_time - departure_time)*24)))), ' km/h') as "Vitesse moyenne",
DECODE( departure_station_id, '1' , 'Bordeaux',
 '2' , 'Brest' ,
 '3' , 'Calais',
 '4' , 'Clermont-Ferrand',
 '5' , 'Dijon',
 '6' , 'Grenoble' ,
 '7' , 'Le Havre',
 '8' , 'Lyon',
 '9' , 'Macon',
 '10' , 'Marseille' ,
 '11' , 'Montpellier',
 '12' , 'Mulhouse',
 '13' , 'Nantes',
 '14' , 'Nice' ,
 '15' , 'Nimes',
 '16' , 'Paris',
 '17' , 'Saint-Malo',
 '18' , 'Strasbourg' ,
 '19' , 'Toulouse',
 '20' , 'Troyes',
 '21' , 'Valenciennes',
 '0') || ' - ' ||
 DECODE( arrival_station_id, '1' , 'Bordeaux',
 '2' , 'Brest' ,
 '3' , 'Calais',
 '4' , 'Clermont-Ferrand',
 '5' , 'Dijon',
 '6' , 'Grenoble' ,
 '7' , 'Le Havre',
 '8' , 'Lyon',
 '9' , 'Macon',
 '10' , 'Marseille' ,
 '11' , 'Montpellier',
 '12' , 'Mulhouse',
 '13' , 'Nantes',
 '14' , 'Nice' ,
 '15' , 'Nimes',
 '16' , 'Paris',
 '17' , 'Saint-Malo',
 '18' , 'Strasbourg' ,
 '19' , 'Toulouse',
 '20' , 'Troyes',
 '21' , 'Valenciennes',
 '0') as "Nom"
 FROM t_train
/

EXIT