SET MARKUP HTML ON SPOOL ON PREFORMAT OFF ENTMAP ON -
HEAD "<TITLE>Report</TITLE> -
<STYLE type='text/css'> -
<!-- BODY {background: #000000} --> -
</STYLE>" -
BODY "TEXT='#00FF00'" -
TABLE "WIDTH='90%' BORDER='5'"

SPOOL report_10.htm

SELECT COUNT(c.last_name) as "Nombre de reduction senior pour les trains qui partent en octobre"
FROM t_customer c
JOIN t_reservation r ON c.customer_id = r.buyer_id
JOIN t_pass p ON c.pass_id = p.pass_id
JOIN t_ticket t ON r.buyer_id = t.customer_id
JOIN t_wagon_train wt ON t.wag_tr_id = wt.wag_tr_id
JOIN t_train tr ON wt.train_id = tr.train_id
WHERE p.pass_id = 2 
AND TO_DATE(tr.departure_time, 'DD/MM/YY') BETWEEN TO_DATE('01/10/20', 'dd/mm/yy') AND LAST_DAY(TO_DATE('01/10/20', 'DD/MM/YY'))
AND r.buy_method IS NOT NULL
/

EXIT