SET MARKUP HTML ON SPOOL ON PREFORMAT OFF ENTMAP ON -
HEAD "<TITLE>Report</TITLE> -
<STYLE type='text/css'> -
<!-- BODY {background: #000000} --> -
</STYLE>" -
BODY "TEXT='#00FF00'" -
TABLE "WIDTH='90%' BORDER='5'"

SPOOL report_07.htm

SELECT CONCAT(CONCAT(s1.city,' - '), s2.city) as "Top 5 trains populaire"
FROM t_train t
JOIN t_station s1 on s1.station_id = t.departure_station_id
JOIN t_station s2 on s2.station_id = t.arrival_station_id
JOIN t_wagon_train wt on t.train_id = wt.train_id
JOIN t_ticket ti on wt.wag_tr_id = ti.wag_tr_id
GROUP BY s1.city, s2.city
ORDER BY(COUNT(ti.reservation_id)) DESC FETCH NEXT 5 ROWS ONLY
/

EXIT