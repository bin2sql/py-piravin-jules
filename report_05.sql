SET MARKUP HTML ON SPOOL ON PREFORMAT OFF ENTMAP ON -
HEAD "<TITLE>Report</TITLE> -
<STYLE type='text/css'> -
<!-- BODY {background: #000000} --> -
</STYLE>" -
BODY "TEXT='#00FF00'" -
TABLE "WIDTH='90%' BORDER='5'"

SPOOL report_05.htm

SELECT t.train_id "Numero du train", t.distance "Distance parcourue", t.price "Prix initial en euros", s1.city || ' (' || TO_CHAR(t.departure_time, 'DD/MM/YY HH24:MI') || ') - ' || s2.city || ' (' || TO_CHAR(t.arrival_time, 'DD/MM/YY HH24:MI') || ')' AS "Trajet"
FROM T_TRAIN t
JOIN T_STATION s1 ON s1.station_id = t.departure_station_id
JOIN T_STATION s2 ON s2.station_id = t.arrival_station_id
ORDER BY t.train_id
/

EXIT