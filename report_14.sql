SET MARKUP HTML ON SPOOL ON PREFORMAT OFF ENTMAP ON -
HEAD "<TITLE>Report</TITLE> -
<STYLE type='text/css'> -
<!-- BODY {background: #000000} --> -
</STYLE>" -
BODY "TEXT='#00FF00'" -
TABLE "WIDTH='90%' BORDER='5'"

SPOOL report_14.htm


SELECT DISTINCT(t.train_id), s1.city || ' - ' || s2.city AS "Train name", w.nb_seat - COUNT(ti.seat) AS "Nombre de places libres"
FROM t_ticket ti
JOIN t_wagon_train tw ON tw.wag_tr_id = ti.wag_tr_id
JOIN t_wagon w ON w.wagon_id = tw.wagon_id
JOIN t_train t ON t.train_id = tw.train_id
JOIN t_station s1 ON s1.station_id = t.departure_station_id
JOIN t_station s2 ON s2.station_id = t.arrival_station_id
WHERE t.distance > 300
AND TO_DATE(t.departure_time, 'DD/MM/YY') = TO_DATE('22/10/20','DD/MM/YY')
AND ti.reservation_id IS NOT NULL
GROUP BY(t.train_id ,s1.city, s2.city, w.nb_seat)
ORDER BY t.train_id
/

EXIT