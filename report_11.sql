SET MARKUP HTML ON SPOOL ON PREFORMAT OFF ENTMAP ON -
HEAD "<TITLE>Report</TITLE> -
<STYLE type='text/css'> -
<!-- BODY {background: #000000} --> -
</STYLE>" -
BODY "TEXT='#00FF00'" -
TABLE "WIDTH='90%' BORDER='5'"

SPOOL report_11.htm

SELECT train_id,CONCAT(CONCAT(s1.city, ' - '), s2.city) "Nom",pass_name,ROUND(( t.price *( 1-(discount_pct/100))),2) normal, ROUND((t.price *( 1-(discount_we_pct/100))),2) weekend
FROM t_train t
JOIN t_station s1 on s1.station_id = t.departure_station_id
JOIN t_station s2 on s2.station_id = t.arrival_station_id
CROSS JOIN t_pass
WHERE s1.station_id = 16
ORDER by train_id
/

EXIT