SET MARKUP HTML ON SPOOL ON PREFORMAT OFF ENTMAP ON -
HEAD "<TITLE>Report</TITLE> -
<STYLE type='text/css'> -
<!-- BODY {background: #000000} --> -
</STYLE>" -
BODY "TEXT='#00FF00'" -
TABLE "WIDTH='90%' BORDER='5'"

SPOOL report_13.htm

SELECT ti.ticket_id, INITCAP(c.first_name) AS prenom , UPPER(c.last_name) AS nom, s1.city || '(' || TO_CHAR(departure_time, 'DD/MM/YY HH24:MI') || ')' || ' - ' || s2.city || '(' || TO_CHAR(arrival_time, 'DD/MM/YY HH24:MI') || ')' as train
FROM t_customer c
JOIN t_reservation r ON r.buyer_id = c.customer_id
JOIN t_ticket ti ON ti.customer_id = r.buyer_id
JOIN t_wagon_train wt ON wt.wag_tr_id = ti.wag_tr_id
JOIN t_wagon w ON w.wagon_id = wt.wagon_id
JOIN t_train t ON t.train_id = wt.train_id
JOIN t_station s1 ON s1.station_id = t.departure_station_id
JOIN t_station s2 ON s2.station_id = t.arrival_station_id
WHERE c.pass_id = 1
AND r.buy_method IS NOT NULL
AND w.class_type = 1
AND TO_DATE(r.creation_date, 'DD/MM/YY') - TO_DATE(t.departure_time, 'DD/MM/YY') > 20
AND TO_DATE(t.departure_time, 'DD/MM/YY') BETWEEN TO_DATE('20/10/20', 'DD/MM/YY') AND TO_DATE('26/10/20', 'DD/MM/YY')
ORDER BY r.creation_date
/

EXIT