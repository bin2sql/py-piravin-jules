SET MARKUP HTML ON SPOOL ON PREFORMAT OFF ENTMAP ON -
HEAD "<TITLE>Report</TITLE> -
<STYLE type='text/css'> -
<!-- BODY {background: #000000} --> -
</STYLE>" -
BODY "TEXT='#00FF00'" -
TABLE "WIDTH='90%' BORDER='5'"

SPOOL report_04.htm

SELECT INITCAP(first_name) AS prenom, UPPER(last_name) AS nom,
	CASE
		WHEN pass_date BETWEEN to_date('14/10/2019','dd/mm/yyyy') AND to_date('14/10/2020','dd/mm/yyyy') THEN 'Valide'
		WHEN pass_date NOT BETWEEN to_date('14/10/2019','dd/mm/yyyy') AND to_date('14/10/2020','dd/mm/yyyy') THEN 'Perime !'
		WHEN pass_date IS NULL THEN 'Aucun'
		end pass
FROM t_customer
ORDER BY last_name, first_name
/


EXIT