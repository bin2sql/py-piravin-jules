SET MARKUP HTML ON SPOOL ON PREFORMAT OFF ENTMAP ON -
HEAD "<TITLE>Report</TITLE> -
<STYLE type='text/css'> -
<!-- BODY {background: #000000} --> -
</STYLE>" -
BODY "TEXT='#00FF00'" -
TABLE "WIDTH='90%' BORDER='5'"

SPOOL report_15.htm

SELECT UPPER(last_name) "Nom",INITCAP(first_name) "Prenom",COUNT(reservation_id)
FROM t_employee e JOIN t_reservation r
ON e.employee_id = r.employee_id
GROUP BY e.employee_id,first_name,last_name
/

EXIT