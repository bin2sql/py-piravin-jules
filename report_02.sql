SET MARKUP HTML ON SPOOL ON PREFORMAT OFF ENTMAP ON -
HEAD "<TITLE>Report</TITLE> -
<STYLE type='text/css'> -
<!-- BODY {background: #000000} --> -
</STYLE>" -
BODY "TEXT='#00FF00'" -
TABLE "WIDTH='90%' BORDER='5'"

SPOOL report_02.htm

SELECT DISTINCT(CONCAT(CONCAT(UPPER(c.last_name), ' '), INITCAP(c.first_name))) AS "Acheteurs reservistes sans billet"
FROM T_TICKET t
JOIN T_RESERVATION r ON t.reservation_id = r.reservation_id
JOIN T_CUSTOMER c ON r.buyer_id = c.customer_id
WHERE r.buyer_id != t.customer_id
ORDER BY CONCAT(CONCAT(UPPER(c.last_name), ' '), INITCAP(c.first_name))
/

EXIT