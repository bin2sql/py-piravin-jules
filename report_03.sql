SET MARKUP HTML ON SPOOL ON PREFORMAT OFF ENTMAP ON -
HEAD "<TITLE>Report</TITLE> -
<STYLE type='text/css'> -
<!-- BODY {background: #000000} --> -
</STYLE>" -
BODY "TEXT='#00FF00'" -
TABLE "WIDTH='90%' BORDER='5'"

SPOOL report_03.htm

SELECT r.reservation_id "Num�ro", r.creation_date "Date", e.last_name "Nom", e.first_name "Pr�nom", c.last_name "Nom", c.first_name "Pr�nom"
FROM t_reservation r JOIN t_employee e
ON r.employee_id = e.employee_id
JOIN t_customer c
ON r.buyer_id = c.customer_id
WHERE r.creation_date = (SELECT MIN(creation_date)
FROM t_reservation)
/

EXIT